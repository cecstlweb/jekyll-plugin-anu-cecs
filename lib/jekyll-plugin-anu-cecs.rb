require 'jekyll'

require 'jekyll-last-modified-at'

require 'jekyll-breadcrumbs/breadcrumbs'
require 'jekyll-plugin-anu-cecs/imagepath'
require 'jekyll-plugin-anu-cecs/modulo.filter'
require 'jekyll-plugin-anu-cecs/relativeurl'

Liquid::Template.register_tag('relativeurl', Jekyll::PluginANUCECS::RelativeURL)
Liquid::Template.register_tag('img_path', Jekyll::PluginANUCECS::ImagePath)
Liquid::Template.register_tag('img', Jekyll::PluginANUCECS::ImageTag)
Liquid::Template.register_filter(Jekyll::ModuloFilter)

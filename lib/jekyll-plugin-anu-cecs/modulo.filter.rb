# Modulo filter for Jekyll

# THIS IS NO LONGER NECESSARY - there's a "modulo" filter in Jekyll's liquid

# Adds modulo functionality to Jekyll. It's already in the Liquid core, but that
# version doesn't appear to be in Jekyll.
#
# That's about it.
#
# https://gist.github.com/leemachin/2366832

module Jekyll
    module ModuloFilter
  
      # Returns the modulo of the input based on the supplied modulus
      # Called 'mod' to avoid conflict with newer Liquid's 'modulo' filter
      def mod(input, modulus)
        puts "this 'mod' filter is deprecated, use 'modulo' instead"
        input.to_i % modulus.to_i
      end
  
    end
  end

require "jekyll-assets"

module Jekyll
  module PluginANUCECS
    class ImagePath < Liquid::Tag
      def initialize(tag, args, tokens)
        @assettag = Jekyll::Assets::Tag.new(tag, args << "@path", tokens)
      end
    
      def render(context)
        relativeurl = RelativeURL.relativeurl(context)
        assetpath = @assettag.render(context)
        baseurl = context.registers[:site].config["baseurl"] || "/"
        assetpath.slice!(0, baseurl.length)

        output = relativeurl << assetpath
        output
      end
    end
    
    class ImageTag < Liquid::Tag
      def initialize(tag, args, tokens)
        @args = Jekyll::Assets::Tag::Parser.new(args)
        @assettag = Jekyll::Assets::Tag.new(tag, args << "@path", tokens)
      end

      def render(context)
        relativeurl = RelativeURL.relativeurl(context)
        assetpath = @assettag.render(context)
        baseurl = context.registers[:site].config["baseurl"] || "/"
        assetpath.slice!(0, baseurl.length) 

        output = "<img " << "src='" << relativeurl << assetpath << "' " << @args.to_html << " />"
        output
      end
    end
  end
end
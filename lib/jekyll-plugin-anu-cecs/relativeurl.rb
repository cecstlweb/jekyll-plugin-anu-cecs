module Jekyll
  module PluginANUCECS
    class RelativeURL < Liquid::Tag
      def self.relativeurl(context)
        linkstyle = context.registers[:page]["linkstyle"] || ""
        url = context.registers[:page]["url"].split('/')
        lastchar = context.registers[:page]["url"][-1]
        baseurl = context.registers[:site].config["baseurl"] || "/"
        getrelativeurl(linkstyle, url, baseurl, lastchar)
      end

      def self.getrelativeurl(linkstyle, url, baseurl, lastchar)
        if linkstyle == "absolute"
          rurl = baseurl
        else
          depth = url.size
          if lastchar != "/"
             depth = depth - 1
          end
          if depth <= 1
            rurl = "."
          else
            rurl = ".."
          end
          for i in 3..depth
            rurl << "/.."
          end
          rurl
        end
      end

      def render(context)
        File.join(RelativeURL.relativeurl(context))
      end
    end
  end

  Hooks.register [:pages, :posts, :documents], :pre_render do |page, payload|
    linkstyle = page['linkstyle'] || ""
    url = page.url.split('/')
    lastchar = page.url[-1]
    baseurl = page.site.baseurl || "/"
    payload["relativeurl"] = PluginANUCECS::RelativeURL::getrelativeurl(linkstyle, url, baseurl, lastchar)
  end
end

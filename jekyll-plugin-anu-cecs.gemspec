Gem::Specification.new do |spec|
  spec.name          = 'jekyll-plugin-anu-cecs'
  spec.version       = "1.3.6"
  spec.authors       = ['David Guest', 'Ben Swift']
  spec.email         = ["helpdesk@cecs.anu.edu.au"]

  spec.summary       = 'ANU CECS Plugin for Jekyll Pages with gitlab'
  spec.homepage      = 'https://gitlab.cecs.anu.edu.au/cecstlweb/jekyll-plugin-anu-cecs'
  spec.licenses      = 'Nonstandard'

  spec.files         = `git ls-files -z lib/`.split("\x0")
  spec.executables   = []
  spec.require_paths = ['lib']

  spec.add_dependency 'jekyll', "~> 3.8"
  spec.add_dependency 'jekyll-assets', "~> 3.0"
  spec.add_dependency 'mini_magick', '~> 4.8'
  spec.add_dependency "jekyll-last-modified-at", "~> 1.0"
  spec.add_dependency "sprockets", "~> 3.7"
end
